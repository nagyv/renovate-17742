To reproduce https://github.com/renovatebot/renovate/discussions/17742

`projects/.terraform.lock.hcl` contains

```hcl
provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.13.0"
  constraints = ">= 3.7.0, < 4.0.0"
  ...
}
```

`projects/main.tf` contains

```hcl
terraform {
  required_version = ">= 0.12.19"
  backend "http" {
  }
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = ">= 3.7.0, <4.0.0"
    }
  }
}
```

Currently, the latest version of [the `gitlabhq/gitlab` provider is 3.18.0](https://registry.terraform.io/providers/gitlabhq/gitlab/)

## How to reproduce?

Run https://gitlab.com/renovate-bot/renovate-runner/ against this repo

## Current behaviour

- No merge request is created, the provider is not updated

## Expected behaviour

- A merge request is created, the provider is updated to `3.18.0`

## Logs

Running the above produced the logs available in the `renovate-log.ndjson` file.
