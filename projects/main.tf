terraform {
  required_version = ">= 0.12.19"
  backend "http" {
  }
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = ">= 3.7.0, <4.0.0"
    }
  }
}

provider "gitlab" {
    token = var.gitlab_token
}

